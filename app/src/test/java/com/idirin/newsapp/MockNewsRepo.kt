package com.idirin.newsapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

import com.google.gson.Gson
import com.idirin.newsapp.data.models.ArticleModel
import com.idirin.newsapp.data.models.NewsResponse
import com.idirin.newsapp.data.repos.helpers.DataHolder
import com.idirin.newsapp.data.repos.interfaces.INewsRepo
import com.idirin.newsapp.utils.JsonUtil

class MockNewsRepo: INewsRepo {

    override fun getNews(): LiveData<DataHolder<List<ArticleModel>>> {
        val ld = MutableLiveData<DataHolder<List<ArticleModel>>>()

        val json = JsonUtil.loadJSONFromAsset("mock_news.json")
        val news = Gson().fromJson(json, NewsResponse::class.java)

        ld.value = DataHolder.success(news.articles)
        return ld
    }

    override fun resetNewsCache() {

    }

    override fun getNewsByWorkManager() {

    }
}