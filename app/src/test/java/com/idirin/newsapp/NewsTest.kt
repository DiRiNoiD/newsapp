package com.idirin.newsapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.idirin.newsapp.data.repos.interfaces.INewsRepo
import com.idirin.newsapp.ui.viewmodels.NewsViewModel
import org.junit.*
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject

/**
 * Created by
 * idirin on 22.09.2018...
 */

class NewsTest: KoinTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val newsViewModel: NewsViewModel by inject()

    private val module = module {
        single { MockNewsRepo() as INewsRepo }
        viewModel { NewsViewModel(get()) }
    }

    @Before
    fun onBefore() {
        startKoin {
            loadKoinModules(listOf(module))
        }
    }

    @After
    fun onAfter() {
        stopKoin()
    }

    @Test
    fun repoTest() {
        val data = newsViewModel.getNews()
        Assert.assertNotNull(data.value)
        Assert.assertEquals(data.value!!.data!!.size, 10)
    }

    @Test
    fun titlesNotNullTest() {
        val data = newsViewModel.getNews().value!!.data!!
        for (new in data) {
            Assert.assertNotNull(new.title)
        }
    }

}