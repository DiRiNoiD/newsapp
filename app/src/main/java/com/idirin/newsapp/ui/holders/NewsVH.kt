package com.idirin.newsapp.ui.holders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_new.view.*

/**
 * Created by
 * idirin on 22.09.2018...
 */

class NewsVH(view: View): RecyclerView.ViewHolder(view) {
    val txtTitle: TextView = view.txtTitle
    val txtDescription: TextView = view.txtDescription
    val imViewContent: ImageView = view.imViewContent
}