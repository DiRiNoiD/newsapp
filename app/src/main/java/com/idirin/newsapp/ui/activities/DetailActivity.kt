package com.idirin.newsapp.ui.activities

import android.os.Build
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.snackbar.Snackbar
import com.idirin.newsapp.R
import com.idirin.newsapp.data.models.ArticleModel
import com.idirin.newsapp.databinding.ActivityDetailBinding
import com.idirin.newsapp.utils.*
import kotlinx.android.synthetic.main.activity_detail.*
import kotlin.math.abs

class DetailActivity : BaseActivity() {

    private var collapsedMenu: Menu? = null
    private var appBarExpanded = true
    private lateinit var handler: Handler
    private lateinit var newsModel: ArticleModel

    private lateinit var binding: ActivityDetailBinding

    override fun setContent() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
    }

    override fun getTitleResId(): Int {
        return R.string.activity_detail
    }

    override fun getBundle() {
        super.getBundle()
        newsModel = intent.getParcelableExtra(Constants.ARG_NEWS)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val imageTransitionName = intent.getStringExtra(Constants.ARG_TRANSITION_NAME)
            imViewContent.transitionName = imageTransitionName
        }

        binding.article = newsModel
        binding.executePendingBindings()
    }

    override fun init() {
        handler = Handler()

        collapsingLayout?.let { ctl ->
            setSupportActionBar(toolbar)

            ctl.setStatusBarScrimColor(ContextCompat.getColor(this, android.R.color.transparent))
            appbar?.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
                if (abs(verticalOffset) - appBarLayout.totalScrollRange == 0) {
                    //  Collapsed
                    ctl.setStatusBarScrimColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
                } else {
                    //Expanded
                    ctl.setStatusBarScrimColor(ContextCompat.getColor(this, android.R.color.transparent))
                }

                //  Vertical offset == 0 indicates appBar is fully expanded.
                if (abs(verticalOffset) > 100) {
                    appBarExpanded = false
                    invalidateOptionsMenu()
                } else {
                    appBarExpanded = true
                    invalidateOptionsMenu()
                }
            })

            var imageRatio = intent.getFloatExtra(Constants.ARG_IMAGE_RATIO, 1f)
            if (imageRatio > 2) {
                imageRatio = 2f
            }
            if (imageRatio < 0.75) {
                imageRatio = 0.75f
            }

            val params = appbar!!.layoutParams
            params.height = (getScreenWidth().toFloat() / imageRatio).toInt()

            fab!!.setOnClickListener { view ->
                shareClicked(view)
            }

            handler.postDelayed({
                fab!!.showScaleAlpha(250)
            }, 400)
        }

        if (supportActionBar != null)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        supportPostponeEnterTransition()

        title = newsModel.title
        imViewContent.loadImage(newsModel.urlToImage)

        //This is for latency in the network or if the image is not cached in Glide
        //After 100 ms the transaction will be done anyway
        handler.postDelayed({
            supportStartPostponedEnterTransition()
        }, 100)
    }

    override fun onBackPressed() {
        fab?.goneScaleAlpha(150)
        handler.postDelayed({
            super.onBackPressed()
        }, 150)
    }

    override fun onDestroy() {
        handler.removeCallbacksAndMessages(null)
        super.onDestroy()
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        if ((collapsedMenu != null && ((!appBarExpanded || collapsingLayout == null) && collapsedMenu!!.size() == 0))) {
            //collapsed
            collapsedMenu!!.add("Add")
                    .setIcon(R.drawable.vector_share)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)
        } else {
            //expanded
        }
        return super.onPrepareOptionsMenu(collapsedMenu)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_detail, menu)
        collapsedMenu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> {
                shareClicked()
            }
        }
        return super.onOptionsItemSelected(item)
    }



    private fun shareClicked(view: View = imViewContent) {
        Snackbar.make(view, getString(R.string.nothing_to_share), Snackbar.LENGTH_LONG).setAction("Action", null).show()
    }
}
