package com.idirin.newsapp.ui.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.idirin.newsapp.R
import com.idirin.newsapp.ui.viewmodels.NewsViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class ListFragment : Fragment() {

    // Lazy inject MyViewModel
    val newsViewModel: NewsViewModel by sharedViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the activity_detail for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false)
    }


}
