package com.idirin.newsapp.ui.activities

import android.content.Context
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItemsSingleChoice
import com.franmontiel.localechanger.LocaleChanger
import com.franmontiel.localechanger.utils.ActivityRecreationHelper
import com.idirin.newsapp.R
import com.idirin.newsapp.ui.adapters.NewsAdapter
import com.idirin.newsapp.ui.viewmodels.NewsViewModel
import com.idirin.newsapp.utils.LocaleUtil
import com.idirin.newsapp.utils.observe
import kotlinx.android.synthetic.main.activity_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListActivity : BaseActivity() {

    // Lazy inject NewsViewModel
    private val newsViewModel: NewsViewModel by viewModel()
    private lateinit var newsAdapter: NewsAdapter

    override fun setContent() {
        setContentView(R.layout.activity_list)
    }

    override fun getTitleResId(): Int {
        return R.string.app_name
    }

    override fun init() {
        newsAdapter = NewsAdapter(this)
        recyclerView.setHasFixedSize(true)

        //Grid view in landscape mode
        //By this method any screen type, orientation, tablet mode can be handled
        //Because tablet design is not wanted I only made landscape view
        //It can be done with adding tablet designs to activity_detail specific directories
        val layoutManager = if (resources.getBoolean(R.bool.useGridOnCards)) {
            GridLayoutManager(this, resources.getInteger(R.integer.cardCount))
        } else {
            LinearLayoutManager(this)
        }
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = newsAdapter

        observeNews()
    }



    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_reset -> {
                resetCaches()
                return true
            }
            R.id.action_locale -> {
                showLocaleSelection()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun attachBaseContext(newBase: Context) {
        val newerBase = LocaleChanger.configureBaseContext(newBase)
        super.attachBaseContext(newerBase)
    }

    override fun onResume() {
        super.onResume()
        ActivityRecreationHelper.onResume(this)
    }

    override fun onDestroy() {
        ActivityRecreationHelper.onDestroy(this)
        super.onDestroy()
    }



    private fun observeNews() {
        newsViewModel.getNews().observe(this,
                onSuccess = { news ->
                    newsAdapter.setItems(news)
                },
                onLoading = {
                    newsAdapter.showLoading()
                },
                onError = { e ->
                    newsAdapter.showError(e.errorMessage)
                })
    }

    private fun resetCaches() {
        newsAdapter.clear()
        newsViewModel.resetCaches()
        observeNews()
    }

    private fun showLocaleSelection() {
        MaterialDialog(this)
                .title(R.string.action_locale)
                .listItemsSingleChoice(R.array.languages, initialSelection = LocaleUtil.getSelectedLanguageIndex(this)) { dialog, index, text ->
                    LocaleUtil.updateResources(this@ListActivity, index)
                    setTitle(R.string.app_name)
                    dialog.dismiss()
                }
                .show()
    }






}
