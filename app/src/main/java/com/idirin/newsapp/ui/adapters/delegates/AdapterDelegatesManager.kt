package com.idirin.newsapp.ui.adapters.delegates

import android.view.ViewGroup
import androidx.collection.SparseArrayCompat
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by
 * idirin on 22.09.2018...
 */

class AdapterDelegatesManager<T> {

    private var delegates: SparseArrayCompat<AdapterDelegate<T>> = SparseArrayCompat()

    fun addDelegate(delegate: AdapterDelegate<T>) {
        delegates.put(delegates.size(), delegate)
    }

    fun getItemViewType(items: T, position: Int): Int {
        for (i in 0..delegates.size()) {
            if (delegates[i]!!.isForViewType(items, position)) {
                return delegates.keyAt(i)
            }
        }
        throw IllegalArgumentException("illegal view type")
    }

    fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegates[viewType]!!.onCreateViewHolder(parent)
    }

    fun onBindViewHolder(items: T, position: Int, holder: RecyclerView.ViewHolder) {
        delegates[holder.itemViewType]!!.onBindViewHolder(items, position, holder)
    }

}