package com.idirin.newsapp.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.idirin.newsapp.data.models.ArticleModel
import com.idirin.newsapp.data.repos.helpers.DataHolder
import com.idirin.newsapp.data.repos.interfaces.INewsRepo

/**
 * Created by
 * idirin on 22.09.2018...
 */

class NewsViewModel(private val newsRepo: INewsRepo) : ViewModel() {

    private var observableNews: LiveData<DataHolder<List<ArticleModel>>> = MutableLiveData()

    init {
        observableNews = newsRepo.getNews()
    }

    fun getNews(): LiveData<DataHolder<List<ArticleModel>>> {
        return observableNews
    }

    fun resetCaches() {
        newsRepo.resetNewsCache()
        observableNews = newsRepo.getNews()
    }

}