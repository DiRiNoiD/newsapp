package com.idirin.newsapp.ui.adapters.delegates

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.idirin.newsapp.R
import com.idirin.newsapp.data.models.LoadingMatchParentModel
import com.idirin.newsapp.ui.adapters.interfaces.IDiffUtil
import com.idirin.newsapp.ui.holders.LoadingMatchParentVH

/**
 * Created by
 * idirin on 22.09.2018...
 */

class LoadingMatchParentAD(private val activity: Activity): AdapterDelegate<List<IDiffUtil>> {

    override fun isForViewType(items: List<IDiffUtil>, position: Int): Boolean {
        return items[position] is LoadingMatchParentModel
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return LoadingMatchParentVH(LayoutInflater.from(parent.context).inflate(R.layout.row_loading_match_parent, parent, false))
    }

    override fun onBindViewHolder(items: List<IDiffUtil>, position: Int, holder: RecyclerView.ViewHolder) {
        val loading = items[position] as LoadingMatchParentModel
        val vh = holder as LoadingMatchParentVH

        //TODO
    }

}