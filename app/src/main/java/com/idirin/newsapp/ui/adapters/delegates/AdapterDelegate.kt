package com.idirin.newsapp.ui.adapters.delegates

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by
 * idirin on 22.09.2018...
 */

interface AdapterDelegate<T> {

    fun isForViewType(items: T, position: Int): Boolean

    fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder

    fun onBindViewHolder(items: T, position: Int, holder: RecyclerView.ViewHolder)

}