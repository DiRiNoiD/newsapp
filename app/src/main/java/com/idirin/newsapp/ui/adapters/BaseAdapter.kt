package com.idirin.newsapp.ui.adapters

import android.app.Activity
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.idirin.newsapp.data.models.*
import com.idirin.newsapp.ui.adapters.delegates.*
import com.idirin.newsapp.ui.adapters.diffutils.BaseDiffUtil
import com.idirin.newsapp.ui.adapters.interfaces.IDiffUtil

/**
 * Created by
 * idirin on 22.09.2018...
 */

@Suppress("UNCHECKED_CAST")
abstract class BaseAdapter<T: IDiffUtil>(private val activity: Activity): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    protected val list: MutableList<T> = ArrayList()
    protected val delegatesManager = AdapterDelegatesManager<List<IDiffUtil>>()

    init {
        delegatesManager.addDelegate(LoadingMatchParentAD(activity))
        delegatesManager.addDelegate(LoadingMatchMoreAD(activity))
        delegatesManager.addDelegate(NotFoundAD(activity))
        delegatesManager.addDelegate(ErrorMatchParentAD(activity))
        delegatesManager.addDelegate(ErrorMoreAD(activity))
    }

    override fun getItemViewType(position: Int): Int {
        return delegatesManager.getItemViewType(list, position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegatesManager.onCreateViewHolder(parent, viewType)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        delegatesManager.onBindViewHolder(list, position, holder)
    }


    abstract fun getNotFoundResId(): Int
    abstract fun getErrorResId(): Int



    open fun showNotFound() {
        removeLoadingMatchParent()
        removeLoadingMore()
        if (list.isEmpty()) {
            list.add(NotFoundModel(activity.getString(getNotFoundResId())) as T)
            notifyItemInserted(0)
        }
    }

    open fun addItem(item: T) {
        removeNotFound()
        list.add(item)
        notifyItemInserted(list.size - 1)
    }

    open fun setItems(itemsToAdd: List<T>) {
        removeLoadingMatchParent()
        removeLoadingMore()

        val oldList: MutableList<T> = ArrayList()
        for (i in 0 until list.size) {
            oldList.add(list[i])
        }
        list.clear()
        list.addAll(itemsToAdd)

        if (list.size == 0) {
            showNotFound()
        } else {
            notify(list, oldList)
        }
    }

    open fun showLoading() {
        if (list.isEmpty()) {
            list.add(LoadingMatchParentModel() as T)
        } else {
            list.add(LoadingMoreModel() as T)
        }
        notifyItemInserted(list.size - 1)
    }

    open fun showError(errorMessage: String) {
        removeLoadingMatchParent()
        removeLoadingMore()
        if (list.isEmpty()) {
            list.add(ErrorMatchParentModel(errorMessage) as T)
            notifyDataSetChanged()
        } else {
            list.add(ErrorMoreRowModel(errorMessage) as T)
            notifyItemInserted(list.size - 1)
        }
    }

    open fun clear() {
        list.clear()
        notifyDataSetChanged()
    }

    private fun notify(newList: List<T>, oldList: List<T>) {
        val diff = DiffUtil.calculateDiff(BaseDiffUtil(oldList, newList))
        diff.dispatchUpdatesTo(this)
    }





    private fun removeLoadingMatchParent() {
        if (list.size > 0) {
            if (list[0] is LoadingMatchParentModel) {
                list.removeAt(0)
                notifyItemRemoved(0)
            }
        }
    }

    private fun removeLoadingMore() {
        if (list.size > 0) {
            if (list[list.size - 1] is LoadingMoreModel) {
                list.removeAt(list.size - 1)
                notifyItemRemoved(list.size - 1)
            }
        }
    }

    private fun removeNotFound() {
        if (list[0] is NotFoundModel) {
            list.removeAt(0)
            notifyItemRemoved(0)
        }
    }





}