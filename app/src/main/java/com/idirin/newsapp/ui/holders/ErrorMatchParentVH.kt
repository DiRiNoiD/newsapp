package com.idirin.newsapp.ui.holders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_error_match_parent.view.*

/**
 * Created by
 * idirin on 22.09.2018...
 */

class ErrorMatchParentVH(view: View): RecyclerView.ViewHolder(view) {
    val txtTitle: TextView = view.txtTitle
    val txtDetail: TextView = view.txtDetail
}