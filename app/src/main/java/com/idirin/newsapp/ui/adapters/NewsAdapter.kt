package com.idirin.newsapp.ui.adapters

import android.app.Activity
import com.idirin.newsapp.R
import com.idirin.newsapp.ui.adapters.delegates.NewsAD
import com.idirin.newsapp.ui.adapters.interfaces.INews

/**
 * Created by
 * idirin on 22.09.2018...
 */

class NewsAdapter(activity: Activity): BaseAdapter<INews>(activity) {

    init {
        delegatesManager.addDelegate(NewsAD(activity))
    }

    override fun getNotFoundResId(): Int {
        return R.string.generic_not_found
    }

    override fun getErrorResId(): Int {
        return R.string.generic_error
    }
}