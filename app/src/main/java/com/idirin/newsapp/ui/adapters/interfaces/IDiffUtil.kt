package com.idirin.newsapp.ui.adapters.interfaces

/**
 * Created by
 * idirin on 22.09.2018...
 */

interface IDiffUtil {
    fun getUniqueId(): String
}