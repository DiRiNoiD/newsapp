package com.idirin.newsapp.ui.adapters.delegates

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.idirin.newsapp.R
import com.idirin.newsapp.data.models.ArticleModel
import com.idirin.newsapp.ui.activities.DetailActivity
import com.idirin.newsapp.ui.adapters.interfaces.IDiffUtil
import com.idirin.newsapp.ui.holders.NewsVH
import com.idirin.newsapp.utils.Constants
import com.idirin.newsapp.utils.loadImage

/**
 * Created by
 * idirin on 22.09.2018...
 */

class NewsAD(private val activity: Activity): AdapterDelegate<List<IDiffUtil>> {

    override fun isForViewType(items: List<IDiffUtil>, position: Int): Boolean {
        return items[position] is ArticleModel
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return NewsVH(LayoutInflater.from(parent.context).inflate(R.layout.row_new, parent, false))
    }

    override fun onBindViewHolder(items: List<IDiffUtil>, position: Int, holder: RecyclerView.ViewHolder) {
        val news = items[position] as ArticleModel
        val vh = holder as NewsVH

        vh.txtTitle.text = news.title
        vh.txtDescription.text = news.description
        vh.imViewContent.loadImage(news.urlToImage)

        vh.itemView.setOnClickListener {

            val bitmap = (vh.imViewContent.drawable as BitmapDrawable).bitmap
            val imageRatio = bitmap.width.toFloat() / bitmap.height.toFloat()

            val intent = Intent(activity, DetailActivity::class.java)
            intent.putExtra(Constants.ARG_NEWS, items[vh.adapterPosition] as ArticleModel)
            intent.putExtra(Constants.ARG_TRANSITION_NAME, ViewCompat.getTransitionName(vh.imViewContent))
            intent.putExtra(Constants.ARG_IMAGE_RATIO, imageRatio)

            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    activity,
                    vh.imViewContent,
                    ViewCompat.getTransitionName(vh.imViewContent)!!
            )

            activity.startActivity(intent, options.toBundle())
        }

    }

}