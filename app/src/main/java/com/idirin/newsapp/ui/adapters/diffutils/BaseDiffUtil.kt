package com.idirin.newsapp.ui.adapters.diffutils

import androidx.recyclerview.widget.DiffUtil
import com.idirin.newsapp.ui.adapters.interfaces.IDiffUtil

/**
 * Created by
 * idirin on 22.09.2018...
 */

class BaseDiffUtil(private val listOld: List<IDiffUtil>, private val listNew: List<IDiffUtil>): DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return listOld[oldItemPosition].getUniqueId() == listNew[newItemPosition].getUniqueId()
    }

    override fun getOldListSize(): Int {
        return listOld.size
    }

    override fun getNewListSize(): Int {
        return listNew.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return listOld[oldItemPosition] == listNew[newItemPosition]
    }
}