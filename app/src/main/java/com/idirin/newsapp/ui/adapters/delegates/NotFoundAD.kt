package com.idirin.newsapp.ui.adapters.delegates

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.idirin.newsapp.R
import com.idirin.newsapp.data.models.NotFoundModel
import com.idirin.newsapp.ui.adapters.interfaces.IDiffUtil
import com.idirin.newsapp.ui.holders.NotFoundVH

/**
 * Created by
 * idirin on 22.09.2018...
 */

class NotFoundAD(private val activity: Activity): AdapterDelegate<List<IDiffUtil>> {

    override fun isForViewType(items: List<IDiffUtil>, position: Int): Boolean {
        return items[position] is NotFoundModel
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return NotFoundVH(LayoutInflater.from(parent.context).inflate(R.layout.row_not_found, parent, false))
    }

    override fun onBindViewHolder(items: List<IDiffUtil>, position: Int, holder: RecyclerView.ViewHolder) {
        val notFound = items[position] as NotFoundModel
        val vh = holder as NotFoundVH

        //TODO
    }

}