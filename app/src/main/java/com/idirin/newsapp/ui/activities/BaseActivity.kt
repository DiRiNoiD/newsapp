package com.idirin.newsapp.ui.activities

import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import com.idirin.newsapp.events.OttoEventBus
import org.koin.android.ext.android.inject

abstract class BaseActivity : AppCompatActivity() {

    /**
     * Here is an example of persistant bus pattern implementation
     * In this application it is not going to be used
     * For just show how to implement from a base activity
     */
    protected val bus: OttoEventBus by inject()

    /**
     * This override method is used to order the logics of the methods
     * and set the content view of the activity
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent()

        bus.register(this)
        setTitle(getTitleResId())
        getBundle()
        init()
    }

    override fun onDestroy() {
        bus.unregister(this)
        super.onDestroy()
    }


    /**
     * Bundle data transactions will be done in this method
     * This method is optional
     */
    open fun getBundle() {}




    abstract fun setContent()

    /**
     * This information can be used to show title of the page or
     * Can be logged for crashlytics or analytics information
     */
    @StringRes
    abstract fun getTitleResId(): Int

    /**
     * This method will be used to init logic of the activity
     */
    abstract fun init()


}