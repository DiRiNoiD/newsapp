package com.idirin.newsapp.ui.adapters.delegates

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.idirin.newsapp.R
import com.idirin.newsapp.data.models.ErrorMatchParentModel
import com.idirin.newsapp.ui.adapters.interfaces.IDiffUtil
import com.idirin.newsapp.ui.holders.ErrorMatchParentVH

/**
 * Created by
 * idirin on 22.09.2018...
 */

class ErrorMatchParentAD(private val activity: Activity): AdapterDelegate<List<IDiffUtil>> {

    override fun isForViewType(items: List<IDiffUtil>, position: Int): Boolean {
        return items[position] is ErrorMatchParentModel
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return ErrorMatchParentVH(LayoutInflater.from(parent.context).inflate(R.layout.row_error_match_parent, parent, false))
    }

    override fun onBindViewHolder(items: List<IDiffUtil>, position: Int, holder: RecyclerView.ViewHolder) {
        val error = items[position] as ErrorMatchParentModel
        val vh = holder as ErrorMatchParentVH

        vh.txtTitle.text = error.message
    }

}