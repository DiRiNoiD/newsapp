package com.idirin.newsapp.di

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

/**
 * Created by
 * idirin on 22.09.2018...
 */

val prefModule = module {
    single { getPref<SharedPreferences>(androidApplication()) }
}

inline fun <reified T> getPref(application: Application): T {
    return PreferenceManager.getDefaultSharedPreferences(application) as T
}