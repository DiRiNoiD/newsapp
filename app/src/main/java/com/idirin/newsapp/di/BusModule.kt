package com.idirin.newsapp.di

import com.idirin.newsapp.events.OttoEventBus
import org.koin.dsl.module

val busModule = module {
    single { OttoEventBus }
}