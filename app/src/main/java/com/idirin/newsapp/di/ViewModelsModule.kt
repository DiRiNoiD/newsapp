package com.idirin.newsapp.di

import com.idirin.newsapp.ui.viewmodels.NewsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by
 * idirin on 22.09.2018...
 */

val viewModelsModule = module {
    viewModel { NewsViewModel(get()) }
}