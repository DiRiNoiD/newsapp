package com.idirin.newsapp.di

import android.app.Application
import androidx.room.Room
import com.idirin.newsapp.BuildConfig
import com.idirin.newsapp.data.AppDb
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

/**
 * Created by
 * idirin on 22.09.2018...
 */

val dbModule = module {
    single { getDb<AppDb>(androidApplication()) }
}

inline fun <reified T> getDb(application: Application): T {
    return Room
            .databaseBuilder(application, AppDb::class.java, BuildConfig.DB_NAME)
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build() as T
}