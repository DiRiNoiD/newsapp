package com.idirin.newsapp.di

import android.app.Application
import com.google.gson.GsonBuilder
import com.idirin.newsapp.BuildConfig
import com.idirin.newsapp.data.ApiInterface
import com.idirin.newsapp.data.TypeAdapterDate
import com.idirin.newsapp.utils.DeviceUtil
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

val apiModule = module {
    single { getApi<ApiInterface>(androidApplication()) }
}

inline fun <reified T> getApi(application: Application): T {

    val gson = GsonBuilder()
            .registerTypeAdapter(Date::class.java, TypeAdapterDate())
            .setLenient()
            .create()

    val cacheSize: Long = 10 * 1024 * 1024  //10mb cache size
    val cache = Cache(application.cacheDir, cacheSize)

    val loggingInterceptor = HttpLoggingInterceptor()
    loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

    val userAgentInterceptor = Interceptor { chain ->
        val request = chain.request()
        val requestWithUserAgent = request.newBuilder()
                .header("User-Agent", DeviceUtil.userAgent)
                .build()
        chain.proceed(requestWithUserAgent)
    }

    val okHttpClient = okhttp3.OkHttpClient.Builder()
            .connectTimeout(BuildConfig.CONNECTION_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
            .readTimeout(BuildConfig.CONNECTION_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
            .writeTimeout(BuildConfig.CONNECTION_UPLOAD_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
            .cache(cache)
            .addInterceptor(loggingInterceptor)
            .addInterceptor(userAgentInterceptor)
            .build()

    return Retrofit.Builder()
            .baseUrl(BuildConfig.REST_ENDPOINT)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .build()
            .create(T::class.java)
}