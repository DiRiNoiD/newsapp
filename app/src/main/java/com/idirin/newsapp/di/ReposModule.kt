package com.idirin.newsapp.di

import com.idirin.newsapp.data.repos.NewsRepo
import com.idirin.newsapp.data.repos.interfaces.INewsRepo
import org.koin.dsl.module

val reposModule = module {
    single { NewsRepo() as INewsRepo }
}