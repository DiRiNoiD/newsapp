package com.idirin.newsapp.events

import android.util.Log
import com.squareup.otto.Bus
import com.squareup.otto.ThreadEnforcer

/**
 * Created by
 * idirin on 22/09/2018...
 */

object OttoEventBus {

    var bus: Bus = Bus(ThreadEnforcer.MAIN, OttoEventBus::class.java.name)

    fun register(o: Any) {
        try {
            bus.register(o)
        } catch (th: Throwable) {
            Log.e("register failed", " $th")
        }
    }

    fun unregister(o: Any) {
        try {
            bus.unregister(o)
        } catch (th: Throwable) {
            Log.e("unregister failed", " $th")
        }
    }

    fun post(e: Any) {
        try {
            bus.post(e)
        } catch (th: Throwable) {
            Log.e("Post failed", " $th")
        }
    }
}
