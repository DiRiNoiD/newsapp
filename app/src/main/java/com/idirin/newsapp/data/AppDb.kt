package com.idirin.newsapp.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.idirin.newsapp.data.daos.NewsDao
import com.idirin.newsapp.data.models.ArticleModel

/**
 * Created by
 * idirin on 22/09/2018...
 */

@Database(entities = [ArticleModel::class], version = 2)
abstract class AppDb : RoomDatabase() {
    abstract fun newsDao(): NewsDao
}
