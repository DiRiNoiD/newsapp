package com.idirin.newsapp.data.daos

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.idirin.newsapp.data.models.ArticleModel

/**
 * Created by
 * idirin on 22/09/2018...
 */

@Dao
interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateNews(news: List<ArticleModel>)

    @Query("select * from articles")
    fun getNews(): List<ArticleModel>

    @Query("select * from articles")
    fun getNewsPaged(): DataSource.Factory<Int, ArticleModel>

    @Query("delete from articles")
    fun deleteAllNews()

}
