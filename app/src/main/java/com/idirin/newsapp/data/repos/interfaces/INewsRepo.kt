package com.idirin.newsapp.data.repos.interfaces

import androidx.lifecycle.LiveData
import com.idirin.newsapp.data.models.ArticleModel
import com.idirin.newsapp.data.repos.helpers.DataHolder

/**
 * Created by
 * idirin on 22.09.2018...
 */

interface INewsRepo {

    fun getNews(): LiveData<DataHolder<List<ArticleModel>>>

    fun resetNewsCache()

    fun getNewsByWorkManager()

}