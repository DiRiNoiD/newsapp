package com.idirin.newsapp.data

import com.idirin.newsapp.data.models.NewsResponse
import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by
 * idirin on 22/09/2018...
 */

interface ApiInterface {

    @GET("v1/articles?source=bbc-news&sortBy=top&apiKey=4dbc17e007ab436fb66416009dfb59a8")
    fun getCSV(): Call<NewsResponse>

}





