package com.idirin.newsapp.data;

import android.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Pattern;

public class TypeAdapterDate implements JsonSerializer<Date>, JsonDeserializer<Date> {

    private static final String DATE_FORMAT =  "yy-M-d";

    private static final long HOUR_IN_MILLISECOND = 60L * 60 * 1000;
    private static final String MINUS_SIGN = "-";
    private static final String PLUS_SIGN = "+";
    private static Pattern pattern = Pattern.compile("^/Date\\([0-9\\+-]*\\)/$");
    private static DecimalFormat formatter = new DecimalFormat("#00.###");

    @Override
    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String date = json.getAsString();
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            Log.e(this.getClass().getSimpleName(), "Failed to parse Date due to:", e);
            return null;
        }
    }

    @Override
    public JsonElement serialize(Date date, Type type, JsonSerializationContext context) {
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        String str = formatter.format(date);
        return new JsonPrimitive(str);
    }
}