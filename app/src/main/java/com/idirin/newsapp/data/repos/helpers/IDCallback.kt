package com.idirin.newsapp.data.repos.helpers

import android.app.Application
import com.idirin.newsapp.R
import com.idirin.newsapp.enums.ErrorType
import com.idirin.newsapp.utils.hasActiveInternetConnection
import org.koin.core.KoinComponent
import org.koin.core.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by
 * idirin on 22/09/2018...
 */

abstract class IDCallback<T> : Callback<T>, KoinComponent {

    private val appContext: Application by inject()

    override fun onResponse(call: Call<T>, response: Response<T>) {

        if (response.code() != 200) {
            //Any backend user friendly message can be handled here
            //There should be two messages in the backend error response
            //1. User Friendly Message
            //2. Developer Message (This will be logged to error search in the future
            onFail(call, NewsError(ErrorType.BACKEND, response.code(), response.errorBody().toString()))
            return
        }

        if (response.body() == null) {
            //Null body is not expected
            //This will be logged if it happens
            onFail(call, NewsError(ErrorType.BACKEND, response.code(), appContext.getString(R.string.generic_error)))
            return
        }

        onSuccess(call, response)
    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        if (appContext.hasActiveInternetConnection()) {
            if (t.message != null) {
                onFail(call, NewsError(ErrorType.SERVICE, -1, t.message!!))
            } else {
                onFail(call, NewsError(ErrorType.SERVICE, -1, appContext.getString(R.string.generic_error)))
            }
        } else {
            onFail(call, NewsError(ErrorType.INTERNET, -1, appContext.getString(R.string.no_internet_error)))
        }
    }

    abstract fun onSuccess(call: Call<T>, response: Response<T>)
    abstract fun onFail(call: Call<T>, error: NewsError)
}