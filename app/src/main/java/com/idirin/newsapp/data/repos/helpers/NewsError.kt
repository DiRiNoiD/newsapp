package com.idirin.newsapp.data.repos.helpers

import com.idirin.newsapp.enums.ErrorType

/**
 * Created by
 * idirin on 22.09.2018...
 */

class NewsError(
        val type: ErrorType,
        val errorCode: Int,
        val errorMessage: String
)







