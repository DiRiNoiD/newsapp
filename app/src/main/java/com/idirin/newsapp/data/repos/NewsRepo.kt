package com.idirin.newsapp.data.repos

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.idirin.newsapp.data.ApiInterface
import com.idirin.newsapp.data.AppDb
import com.idirin.newsapp.data.models.ArticleModel
import com.idirin.newsapp.data.models.NewsResponse
import com.idirin.newsapp.data.repos.helpers.DataHolder
import com.idirin.newsapp.data.repos.helpers.IDCallback
import com.idirin.newsapp.data.repos.helpers.NewsError
import com.idirin.newsapp.data.repos.interfaces.INewsRepo
import org.koin.core.KoinComponent
import org.koin.core.inject
import retrofit2.Call
import retrofit2.Response

/**
 * Created by
 * idirin on 22.09.2018...
 */

class NewsRepo: INewsRepo, KoinComponent {

    //Lazy injects
    private val api: ApiInterface by inject()
    private val db: AppDb by inject()


    override fun getNews(): LiveData<DataHolder<List<ArticleModel>>> {
        val ld = MutableLiveData<DataHolder<List<ArticleModel>>>()
        ld.value = DataHolder.loading()

        //Offline Usage from Cached News
        val newsList: MutableList<ArticleModel> = ArrayList()
        newsList.addAll(db.newsDao().getNews())
        if (newsList.isNotEmpty()) {
            ld.value = DataHolder.success(newsList)
        }

        api.getCSV().enqueue(object : IDCallback<NewsResponse>() {
            override fun onSuccess(call: Call<NewsResponse>, response: Response<NewsResponse>) {
                updateDbFromCsv(response.body()!!)
                ld.value = DataHolder.success(db.newsDao().getNews())
            }

            override fun onFail(call: Call<NewsResponse>, error: NewsError) {
                ld.value = DataHolder.error(error, null)
            }
        })
        return ld
    }

    override fun resetNewsCache() {
        db.newsDao().deleteAllNews()
    }

    override fun getNewsByWorkManager() {
        api.getCSV().enqueue(object : IDCallback<NewsResponse>() {
            override fun onSuccess(call: Call<NewsResponse>, response: Response<NewsResponse>) {
                updateDbFromCsv(response.body()!!)
            }

            override fun onFail(call: Call<NewsResponse>, error: NewsError) {
                //TODO
                //Error needs to be logged
            }
        })
    }


    private fun updateDbFromCsv(responseBody: NewsResponse) {
        db.newsDao().updateNews(responseBody.articles)
    }

}