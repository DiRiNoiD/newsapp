package com.idirin.newsapp.data.models

import com.idirin.newsapp.ui.adapters.interfaces.IDiffUtil

/**
 * Created by
 * idirin on 22.09.2018...
 */

class LoadingMatchParentModel: IDiffUtil {
    override fun getUniqueId(): String {
        return "LoaderMatchParentModel"
    }
}

class LoadingMoreModel: IDiffUtil {
    override fun getUniqueId(): String {
        return "LoaderMoreModel"
    }
}






