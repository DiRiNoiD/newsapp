package com.idirin.newsapp.data.repos.helpers

/**
 * Created by
 * idirin on 22/09/2018...
 */

class DataHolder<out T> private constructor(val status: RepositoryStatus, val data: T?, val error: NewsError?) {

    companion object {

        fun <T> success(data: T): DataHolder<T> {
            return DataHolder(RepositoryStatus.OK, data, null)
        }

        fun <T> error(error: NewsError, data: T?): DataHolder<T> {
            return DataHolder(RepositoryStatus.ERROR, data, error)
        }

        fun <T> loading(): DataHolder<T> {
            return DataHolder(RepositoryStatus.LOADING, null, null)
        }
    }
}
