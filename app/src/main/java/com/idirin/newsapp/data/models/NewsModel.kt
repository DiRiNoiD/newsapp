package com.idirin.newsapp.data.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.idirin.newsapp.ui.adapters.interfaces.INews
import kotlinx.android.parcel.Parcelize

/**
 * Created by
 * idirin on 22.09.2018...
 */

data class NewsResponse(
        val articles: List<ArticleModel>,
        val sortBy: String,
        val source: String,
        val status: String
)

@Entity(tableName = "articles")
@Parcelize
data class ArticleModel(
        @PrimaryKey(autoGenerate = true) val id: Int,
        val author: String,
        val description: String,
        val publishedAt: String,
        val title: String,
        val url: String,
        val urlToImage: String
) : Parcelable, INews {
    override fun getUniqueId(): String {
        return id.toString()
    }
}