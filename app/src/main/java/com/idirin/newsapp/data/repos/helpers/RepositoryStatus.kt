package com.idirin.newsapp.data.repos.helpers

/**
 * Created by
 * idirin on 05/12/2017...
 */

enum class RepositoryStatus {
    OK, ERROR, LOADING
}
