package com.idirin.newsapp.enums

/**
 * Created by
 * idirin on 21.03.2018...
 */

enum class ErrorType {
    SERVICE, INTERNET, OTHER, BACKEND
}