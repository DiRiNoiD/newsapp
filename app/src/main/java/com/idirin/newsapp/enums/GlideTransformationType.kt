package com.idirin.newsapp.enums

/**
 * Created by
 * idirin on 22.09.2018...
 */

enum class GlideTransformationType {
    CIRCLE, NONE
}