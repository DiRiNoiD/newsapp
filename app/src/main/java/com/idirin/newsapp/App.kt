package com.idirin.newsapp

import android.content.Context
import android.content.res.Configuration
import android.util.Log
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import androidx.work.*
import com.franmontiel.localechanger.LocaleChanger
import com.idirin.newsapp.di.*
import com.idirin.newsapp.service.BackGroundUpdater
import com.idirin.newsapp.utils.LocaleUtil
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by
 * idirin on 22.09.2018...
 */

class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()

        LocaleChanger.initialize(applicationContext, listOf(Locale("en"), Locale("de"), Locale("nl"), Locale("tr")))

        startKoin {
            androidContext(this@App)
            modules(listOf(busModule, apiModule, reposModule, viewModelsModule, prefModule, dbModule))
        }

        LocaleUtil.updateApplicationLocale(this)

        initBackGroundUpdater()
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        LocaleChanger.onConfigurationChanged()
    }


    private fun initBackGroundUpdater() {
        Log.i("BackGroundUpdater", "BackGroundUpdater scheduled")
        WorkManager.getInstance(this).cancelAllWork()

        val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

        val updateWork = PeriodicWorkRequestBuilder<BackGroundUpdater>(PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS, TimeUnit.MILLISECONDS, PeriodicWorkRequest.MIN_PERIODIC_FLEX_MILLIS, TimeUnit.MILLISECONDS)
                .setConstraints(constraints)
                .build()

        WorkManager.getInstance(this).enqueue(updateWork)
    }

}