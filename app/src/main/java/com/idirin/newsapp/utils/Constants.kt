package com.idirin.newsapp.utils

object Constants {

    const val PHOTO_FETCH_ATTEMPT_COUNT = 5

    const val ARG_NEWS = "argNews"
    const val ARG_TRANSITION_NAME = "argTransitionName"
    const val ARG_IMAGE_RATIO = "argImageRatio"

    const val DEFAULT_PAGE_SIZE = 10

}