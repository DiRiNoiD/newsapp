package com.idirin.newsapp.utils

import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.load.Key
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import org.jetbrains.anko.doAsync

/**
 * Created by idirin on 22/09/2018.
 * Glide Interceptor Customizations
 */
object GlideUtil {

    fun getGlideUrl(imageUrl: String): GlideUrl {
        return GlideUrl(imageUrl, LazyHeaders.Builder()
                .addHeader("User-Agent", DeviceUtil.userAgent)
                .build())
    }

    fun clearMemoryAndCache(context: Context) {
        // This method must be called on the main thread.
        Glide.get(context).clearMemory()
        doAsync {
            // This method must be called async
            Glide.get(context).clearDiskCache()
        }
    }

    fun getSignature(imageUrl: String): Key {
        return StringSignature(imageUrl)
    }

}
