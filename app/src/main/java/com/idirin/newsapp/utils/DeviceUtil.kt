package com.idirin.newsapp.utils

import android.os.Build
import com.idirin.newsapp.BuildConfig

object DeviceUtil {

    val userAgent: String
        get() = ("Android/" + BuildConfig.APPLICATION_ID + "/" + BuildConfig.VERSION_CODE + "/("
                + Build.MANUFACTURER + ":" + Build.VERSION.RELEASE + ":" + Build.MODEL + ")")

}
