package com.idirin.newsapp.utils

import android.animation.Animator
import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.ContextWrapper
import android.graphics.Point
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.os.Build
import android.os.Handler
import android.text.TextUtils
import android.view.*
import android.view.View.*
import android.view.animation.AccelerateInterpolator
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.idirin.newsapp.R
import com.idirin.newsapp.data.repos.helpers.DataHolder
import com.idirin.newsapp.data.repos.helpers.NewsError
import com.idirin.newsapp.data.repos.helpers.RepositoryStatus
import com.idirin.newsapp.enums.GlideTransformationType

/**
 * Created by
 * idirin on 22/09/2018...
 */

inline fun <T> LiveData<DataHolder<T>>.observe(
        owner: LifecycleOwner,
        crossinline onSuccess: (data: T) -> Unit,
        noinline onLoading: (() -> Unit)? = null,
        noinline onError: ((e: NewsError) -> Unit)? = null)
{
    this.observe(owner, Observer {
        if (it == null) {
            throw RuntimeException("null data holder")
        }

        when (it.status) {
            RepositoryStatus.OK -> {
                onSuccess(this.value!!.data!!)
            }
            RepositoryStatus.LOADING -> {
                if (onLoading == null) {
                    //Default behaviour for example circular dialog progress
                } else {
                    onLoading()
                }
            }
            RepositoryStatus.ERROR -> {
                if (onError == null) {
                    //Default behaviour on error for example pop back
                    when (owner) {
                        is Activity -> {
                            owner.onBackPressed()
                        }
                        is Fragment -> {
                            owner.activity?.onBackPressed()
                        }
                        else -> throw NotImplementedError(owner.toString())
                    }
                } else {
                    onError(this.value!!.error!!)
                }
            }
        }
    })
}


fun <T: Activity> T.getScreenWidth(): Int {
    val display = windowManager.defaultDisplay
    val size = Point()
    display.getSize(size)
    return size.x
}

fun Application.hasActiveInternetConnection(): Boolean {
    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork = cm.activeNetworkInfo
    return activeNetwork != null
}


inline fun View.waitForLayout(crossinline f: () -> Unit) = with(viewTreeObserver) {
    addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            removeOnGlobalLayoutListener(this)
            f()
        }
    })
}

inline fun <T: View> T.afterMeasured(crossinline f: T.() -> Unit) {
    viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            if (measuredWidth > 0 && measuredHeight > 0) {
                viewTreeObserver.removeOnGlobalLayoutListener(this)
                f()
            }
        }
    })
}




val ViewGroup.childViews: List<View>
    get() = (0 until childCount).map{ getChildAt(it) }

val ViewGroup.visibleChildCount: Int
    get() {
        var visibleChildCount = 0
        for (i in 0 until childCount) {
            if (getChildAt(i).visibility == View.VISIBLE) {
                visibleChildCount++
            }
        }
        return visibleChildCount
    }



fun Fragment.toast(message: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(activity, message, duration).show()
}

fun Activity.toast(message: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, duration).show()
}

fun Fragment.toast(stringResId: Int, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(activity, stringResId, duration).show()
}

fun Activity.toast(stringResId: Int, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, stringResId, duration).show()
}




fun EditText.string(): String {
    return text.toString()
}

fun TextView.string(): String {
    return text.toString()
}



fun ImageView.setVectorColorResId(@ColorRes colorResId: Int) {
    setColorFilter(ContextCompat.getColor(context, colorResId))
}

fun ImageView.setVectorColor(@ColorInt colorId: Int) {
    setColorFilter(colorId)
}



fun View.remove() {
    if (this.parent != null) {
        (this.parent as ViewGroup).removeView(this)
    }
}



fun View.showAlpha(duration: Long = 500) {
    visible()
    val animator = ObjectAnimator.ofFloat(this, "alpha", 1f)
    animator.duration = duration
    animator.start()
}

fun View.hideAlpha(duration: Long = 500) {
    val animator = ObjectAnimator.ofFloat(this, "alpha", 0f)
    animator.duration = duration
    animator.start()
}

fun View.goneAlpha(duration: Long = 500) {
    val animator = ObjectAnimator.ofFloat(this, "alpha", 0f)
    animator.duration = duration
    animator.addListener(object : Animator.AnimatorListener {
        override fun onAnimationRepeat(animation: Animator?) {

        }

        override fun onAnimationEnd(animation: Animator?) {
            this@goneAlpha.gone()
        }

        override fun onAnimationCancel(animation: Animator?) {
        }

        override fun onAnimationStart(animation: Animator?) {
        }
    })
    animator.start()
}

fun View.goneScaleAlpha(duration: Long = 500) {
    val phScaleX = PropertyValuesHolder.ofFloat(SCALE_X, 0f)
    val phScaleY = PropertyValuesHolder.ofFloat(SCALE_Y, 0f)
    val phAlpha = PropertyValuesHolder.ofFloat(ALPHA, 0f)
    val animator = ObjectAnimator.ofPropertyValuesHolder(this, phScaleX, phScaleY, phAlpha)
    animator.duration = duration
    animator.addListener(object : Animator.AnimatorListener {
        override fun onAnimationRepeat(animation: Animator?) {

        }

        override fun onAnimationEnd(animation: Animator?) {
            this@goneScaleAlpha.gone()
        }

        override fun onAnimationCancel(animation: Animator?) {
        }

        override fun onAnimationStart(animation: Animator?) {
        }
    })
    animator.start()
}

fun View.showScaleAlpha(duration: Long = 500) {
    visible()
    val phScaleX = PropertyValuesHolder.ofFloat(SCALE_X, 1f)
    val phScaleY = PropertyValuesHolder.ofFloat(SCALE_Y, 1f)
    val phAlpha = PropertyValuesHolder.ofFloat(ALPHA, 1f)
    val animator = ObjectAnimator.ofPropertyValuesHolder(this, phScaleX, phScaleY, phAlpha)
    animator.duration = duration
    animator.start()
}



fun View.visible() {
    this.visibility = View.VISIBLE
    this.invalidate()
    this.requestLayout()
}

fun View.inVisible() {
    this.visibility = View.INVISIBLE
    this.invalidate()
    this.requestLayout()
}

fun View.gone() {
    this.visibility = View.GONE
    this.invalidate()
    this.requestLayout()
}

fun View.showCircular(viewGroup: ViewGroup, centerX: Int = 0, centerY: Int = 0) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        val startRadius = 0
        val endRadius = Math.hypot(viewGroup.width.toDouble(), viewGroup.height.toDouble()).toInt()
        val anim = ViewAnimationUtils.createCircularReveal(this, centerX, centerY, startRadius.toFloat(), endRadius.toFloat())
        this.visible()
        anim.start()
    } else {
        this.visible()
    }
}

fun View.hideCircular(viewGroup: ViewGroup, centerX: Int = 0, centerY: Int = 0) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        val startRadius = Math.max(viewGroup.width, viewGroup.height)
        val endRadius = 0
        val anim = ViewAnimationUtils.createCircularReveal(this, centerX, centerY, startRadius.toFloat(), endRadius.toFloat())
        anim.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {}

            override fun onAnimationEnd(animation: Animator?) {
                this@hideCircular.gone()
            }

            override fun onAnimationCancel(animation: Animator?) {}

            override fun onAnimationStart(animation: Animator?) {}
        })
        anim.start()
    } else {
        this.gone()
    }
}

fun View.rotateHorizontal(toDegree: Float, duration: Long = 400) {
    clearAnimation()
    val objectAnimator = ObjectAnimator.ofFloat(this, "rotationX", rotationX, toDegree)
    objectAnimator.duration = duration
    objectAnimator.interpolator = AccelerateInterpolator()
    objectAnimator.start()
}








//TODO
//Glide default place holder and error
fun ImageView.loadImage(path: String?,
                        placeholder: Int = R.drawable.placeholder,
                        errorholder: Int = R.drawable.placeholder,
                        attemptCount: Int = 0,
                        transformation: GlideTransformationType = GlideTransformationType.NONE) {


    if (context == null) {
        return
    } else if (context !is Application) {

        val contextToCheck = if (context is ContextWrapper) {
            (context as ContextWrapper).baseContext
        } else {
            context
        }

        if (contextToCheck is FragmentActivity) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && contextToCheck.isDestroyed) {
                return
            } else if (contextToCheck.isFinishing) {
                return
            }
        } else if (contextToCheck is Activity) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && contextToCheck.isDestroyed) {
                return
            } else if (contextToCheck.isFinishing) {
                return
            }
        }
    }


    val errorResId = if (attemptCount == Constants.PHOTO_FETCH_ATTEMPT_COUNT - 1) {
        errorholder
    } else {
        placeholder
    }
    var reAttemptCount = attemptCount


    val securePath = if (!TextUtils.isEmpty(path)) {
        path!!.replace("http://", "https://")
    } else {
        errorholder.toString()
    }

    val options = RequestOptions()
            .fitCenter()
            .placeholder(placeholder)
            .signature(GlideUtil.getSignature(securePath))
            .error(errorResId)
            .diskCacheStrategy(DiskCacheStrategy.ALL)

    when (transformation) {
        GlideTransformationType.CIRCLE -> options.circleCrop()
        else -> {}
    }

    if (TextUtils.isEmpty(path)) {
        Glide
                .with(context)
                .load(errorholder)
                .apply(options)
                .into(this)

        if ((context as ContextThemeWrapper).baseContext is FragmentActivity) {
            ((context as ContextThemeWrapper).baseContext as FragmentActivity).supportStartPostponedEnterTransition()
        }
        return
    }

    Glide
            .with(context)
            .load(GlideUtil.getGlideUrl(securePath))
            .apply(options)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                    reAttemptCount++
                    if (reAttemptCount < Constants.PHOTO_FETCH_ATTEMPT_COUNT) {
                        return if (reAttemptCount == Constants.PHOTO_FETCH_ATTEMPT_COUNT - 1) {
                            Handler().post{
                                loadImage(path, placeholder, errorholder, reAttemptCount + 1, transformation)
                            }
                            false
                        } else {
                            Handler().post{
                                loadImage(path, placeholder, errorholder, reAttemptCount + 1, transformation)
                            }
                            false
                        }
                    }

                    //TODO
                    //Log Error

                    if ((context as ContextThemeWrapper).baseContext is FragmentActivity) {
                        ((context as ContextThemeWrapper).baseContext as FragmentActivity).supportStartPostponedEnterTransition()
                    }

                    return false
                }

                override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                    if ((context as ContextThemeWrapper).baseContext is FragmentActivity) {
                        ((context as ContextThemeWrapper).baseContext as FragmentActivity).supportStartPostponedEnterTransition()
                    }
                    return false
                }
            })
            .into(this)
}