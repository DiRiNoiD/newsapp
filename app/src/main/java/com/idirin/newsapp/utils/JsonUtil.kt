package com.idirin.newsapp.utils

import java.io.IOException
import java.nio.charset.Charset

object JsonUtil {

    fun loadJSONFromAsset(fileName: String): String? {
        val json: String
        try {
            val `is` = javaClass.classLoader.getResourceAsStream(fileName)

            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            json = String(buffer, Charset.defaultCharset())
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }

}