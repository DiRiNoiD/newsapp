package com.idirin.newsapp.utils

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import com.franmontiel.localechanger.LocaleChanger
import com.franmontiel.localechanger.utils.ActivityRecreationHelper
import com.idirin.newsapp.R
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.util.*


/**
 * Created by
 * idirin on 22.09.2018...
 */


object LocaleUtil : KoinComponent {

    private val pref by inject<SharedPreferences>()

    val PREFS_LANGUAGE = "LANGUAGE"

    fun updateResources(activity: Activity, index: Int) {
        val languageCodes = activity.resources.getStringArray(R.array.languageCodes)
        persist(languageCodes[index])
        val locale = Locale(languageCodes[index])
        LocaleChanger.setLocale(locale)
        updateApplicationLocale(activity)
        ActivityRecreationHelper.recreate(activity, true)
    }

    fun getSelectedLanguageIndex(context: Context): Int {
        val languageCode = pref.getString(PREFS_LANGUAGE, "en")
        val languageCodes = context.resources.getStringArray(R.array.languageCodes)
        return languageCodes.indexOf(languageCode)
    }

    fun getLocale(context: Context): Locale {
        val languageCodes = context.resources.getStringArray(R.array.languageCodes)
        return Locale(languageCodes[getSelectedLanguageIndex(context)])
    }

    private fun persist(languageCode: String) {
        pref.edit().putString(PREFS_LANGUAGE, languageCode).apply()
    }

    fun updateApplicationLocale(context: Context) {
        val locale = getLocale(context)
        val config = context.resources.configuration
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(locale)
        } else {
            config.locale = locale
        }
        context.applicationContext.resources.updateConfiguration(
                config,
                context.applicationContext.resources.displayMetrics
        )
        Locale.setDefault(locale)
    }

}