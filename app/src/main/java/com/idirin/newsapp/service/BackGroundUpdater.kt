package com.idirin.newsapp.service

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.idirin.newsapp.data.repos.interfaces.INewsRepo
import org.koin.core.KoinComponent
import org.koin.core.inject

/**
 * Created by
 * idirin on 23.09.2018...
 */

class BackGroundUpdater(context: Context, workerParams: WorkerParameters): Worker(context, workerParams), KoinComponent {

    companion object {
        private const val TAG = "BackGroundUpdater"
    }

    private val newsRepo: INewsRepo by inject()

    override fun doWork(): Result {
        Log.i(TAG, "BackGroundUpdater started")
        newsRepo.getNewsByWorkManager()
        return Result.success()
    }
}