package com.idirin.newsapp

import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.rule.ActivityTestRule
import com.idirin.newsapp.ui.activities.ListActivity
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by
 * idirin on 22.09.2018...
 */


@LargeTest
@RunWith(AndroidJUnit4::class)
class UITest {

    @get:Rule
    val mActivityTestRule = ActivityTestRule(ListActivity::class.java, true, false)

    companion object {
        @BeforeClass
        @JvmStatic
        fun onBeforeClass() {
            TestUtils.setPreRequirements(InstrumentationRegistry.getInstrumentation().targetContext)
        }
    }

    @Before
    fun onBefore() {
        mActivityTestRule.launchActivity(Intent())
    }

    @Test
    fun listItemTest() {
        Thread.sleep(1000)
        for (i in 0 until 5) {
            onView(withId(R.id.recyclerView)).perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(i, click()))
            Thread.sleep(1000)
            pressBack()
            Thread.sleep(1000)
        }
    }

    @Test
    fun resetText() {
        openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)
        onView(withText("Reset Cache")).perform(click())

        Thread.sleep(1000)
        for (i in 0 until 5) {
            onView(withId(R.id.recyclerView)).perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(i, click()))
            Thread.sleep(1000)
            pressBack()
            Thread.sleep(1000)
        }
    }

    @Test
    fun localizationTest() {

        //NL
        Thread.sleep(500)
        openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)
        onView(withText("Select Language")).perform(click())
        Thread.sleep(500)
        onView(withText("Dutch")).perform(click())

        //EN
        Thread.sleep(500)
        openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)
        onView(withText("Selecteer Taal")).perform(click())
        Thread.sleep(500)
        onView(withText("Turks")).perform(click())

        //TR
        Thread.sleep(500)
        openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)
        onView(withText("Dil Seçiniz")).perform(click())
        Thread.sleep(500)
        onView(withText("İngilizce")).perform(click())

        Thread.sleep(500)
    }







}