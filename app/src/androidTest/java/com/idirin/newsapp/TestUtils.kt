package com.idirin.newsapp

import android.content.Context
import android.preference.PreferenceManager
import com.franmontiel.localechanger.LocaleChanger
import com.idirin.newsapp.utils.LocaleUtil

/**
 * Created by
 * idirin on 22.09.2018...
 */

object TestUtils {

     fun setPreRequirements(context: Context) {
         val preferences = PreferenceManager.getDefaultSharedPreferences(context)
         val editor = preferences.edit()
         editor.putString(LocaleUtil.PREFS_LANGUAGE, "en").apply()
         LocaleChanger.setLocale(LocaleUtil.getLocale(context))
     }

}