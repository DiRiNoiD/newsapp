package com.idirin.newsapp

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.google.gson.Gson
import com.idirin.newsapp.data.AppDb
import com.idirin.newsapp.data.daos.NewsDao
import com.idirin.newsapp.data.models.NewsResponse
import com.idirin.newsapp.utils.JsonUtil
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by
 * idirin on 22.09.2018...
 */

@RunWith(AndroidJUnit4::class)
class RoomDbTest {

    private lateinit var newsDao: NewsDao
    private lateinit var appDb: AppDb

    @Before
    fun onBefore() {
        val context = InstrumentationRegistry.getInstrumentation().context
        appDb = Room.inMemoryDatabaseBuilder(context, AppDb::class.java).build()
        newsDao = appDb.newsDao()
        fillDb()
    }

    @After
    fun onAfter() {
        appDb.close()
    }


    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        Assert.assertTrue(appContext.packageName.contains("com.idirin.newsapp"))
        Assert.assertTrue(appContext.packageName.contains("debug"))
    }


    @Test
    fun getNewsTest() {
        val news = newsDao.getNews()
        Assert.assertTrue(news.isNotEmpty())
    }

    @Test
    fun deleteDbTest() {
        newsDao.deleteAllNews()
        val news = newsDao.getNews()
        Assert.assertTrue(news.isEmpty())
    }

    @Test
    fun updateNewsTest() {
        val newsSize = newsDao.getNews().size
        fillDb()
        val updateNewsSize = newsDao.getNews().size
        Assert.assertEquals(newsSize, updateNewsSize)
    }



    private fun fillDb() {
        val json = JsonUtil.loadJSONFromAsset("mock_news.json")
        val news = Gson().fromJson(json, NewsResponse::class.java)
        newsDao.deleteAllNews()
        newsDao.updateNews(news.articles)
    }

}