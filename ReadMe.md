# News App

NewsApp is an online news app that shows a list of news and detail for each.

## Architecture

MVVM is used for architectural pattern with Google architecture components such as livedata and viewmodel. Room is used for caching news data and Glide is used for caching image data. Koin is used for dependency injection which has considerable advantages over Dagger. Bus pattern implemented with Otto but not used.

With these components MVVM pattern implemented which used observable and repository pattern. With the livedata, view weakly (causes no data leakage) observes viewModel data changes, which is triggered by the repository. Repository pattern is used for any data transaction from local or remote source but the view does not know where the data is coming from so it can be mockable, replacable and more maintainable.

Adapter Delegate Pattern is used for designing list adapter which provides a portable structure for using any combination of the delegates so easyly. In this app a base adapter is designed for handling any loading, error, or success states and can be reusable with any adapter with desired combination.

## UI Structure

Screen rotations, configuration changes handled in the app. Because state changes are handled with viewModels, save and restore instance states are not used. Shared component transition used for the screen transition animations. Two different layout and layoutManager used for the screen orientation. Only activities used for the screens, no fragment used. But if you would want tablet design, a common approach would be master detail implementation with fragments with a structure of shared viewModel.

For UX, Material design components are used. Simple recyclerview and detail page with parrallax scrollable content are used.

## Performance

For the best performance data operations are done in background thread once, then they are presented instantly offline for not to wait user until api responses the request. After then DiffUtil is used to refresh recyclerview content with the lowest cost of refreshing when the api responses.

## Error Handling

NewsError class defined for handling errors caused by remote data source. The incoming response investigated in SmartCallback and can be decided what to do with that error. 

## Offline Mode / Caching Strategies

For data caching Room ORM database is used. After a refresh from the api local database is also refreshed with minimum effect on user. For image caching Glide result caching is used with a url based signature which ensures the same urls are gotten from cache. Caching reset feature is added to see behaviour of the app without internet connection.

A workerManager is implemented to download news from background even if the app is not working. By this method user always will see the updated news list without any time delay cause by the remote data source. WorkerManager has scheduled on app open and work when device connects to internet.

## Localisation

4 different languages applied which are Dutch, English, German and Turkish and can be used in runtime.

### Unit and UI Tests

There are 3 types of tests implemented. Unit, UI and instrumented Unit tests. Koin DI is used for mocking data source.
Unit tests checks the client logic of the app.
UI test checks the whole application flow.
Instrumented unit tests check the local storage of the app.

With these test, an implementation of CI can be set up so in every build whole app and the client logic can be checked and Continuous Deployment also can be applied with a CI tool such as Jenkins, TeamCity, CircleCI. 




Ilyas Dirin
